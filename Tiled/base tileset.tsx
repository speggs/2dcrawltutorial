<?xml version="1.0" encoding="UTF-8"?>
<tileset name="base tileset" tilewidth="16" tileheight="16" spacing="1" tilecount="1767" columns="57">
 <image source="../Assets/Art/roguelikeSheet_transparent.png" width="968" height="526"/>
 <terraintypes>
  <terrain name="Dirt" tile="578"/>
  <terrain name="Water" tile="60">
   <properties>
    <property name="unity:sortingLayerName" value="Water"/>
   </properties>
  </terrain>
  <terrain name="Grass" tile="915"/>
  <terrain name="Red Earth" tile="1086"/>
  <terrain name="Purple Earth" tile="1257"/>
  <terrain name="White Stone" tile="1262"/>
  <terrain name="Grey Stone" tile="920"/>
  <terrain name="Pond" tile="231"/>
  <terrain name="Red Flower Garden" tile="402"/>
  <terrain name="White Flower Garden" tile="573"/>
  <terrain name="Blue Flower Garden" tile="744"/>
 </terraintypes>
 <tile id="2" terrain=",,,1">
  <objectgroup draworder="index">
   <object id="4" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="3" terrain=",,1,1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="4" terrain=",,1,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="29">
  <objectgroup draworder="index">
   <object id="3" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="57" terrain="1,1,1,">
  <objectgroup draworder="index">
   <object id="3" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="58" terrain="1,1,,1">
  <objectgroup draworder="index">
   <object id="4" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="59" terrain=",1,,1">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="60" terrain="1,1,1,1">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="61" terrain="1,,1,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="71">
  <objectgroup draworder="index">
   <object id="3" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="114" terrain="1,,1,1">
  <objectgroup draworder="index">
   <object id="5" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="115" terrain=",1,1,1">
  <objectgroup draworder="index">
   <object id="3" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="116" terrain=",1,,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="117" terrain="1,1,,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="118" terrain="1,,,">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="171" terrain="7,7,7,"/>
 <tile id="172" terrain="7,7,,7"/>
 <tile id="173" terrain=",,,7"/>
 <tile id="174" terrain=",,7,7"/>
 <tile id="175" terrain=",,7,"/>
 <tile id="228" terrain="7,,7,7"/>
 <tile id="229" terrain=",7,7,7"/>
 <tile id="230" terrain=",7,,7"/>
 <tile id="231" terrain="7,7,7,7"/>
 <tile id="232" terrain="7,,7,"/>
 <tile id="287" terrain=",7,,"/>
 <tile id="288" terrain="7,7,,"/>
 <tile id="289" terrain="7,,,"/>
 <tile id="296">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="297">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="306">
  <objectgroup draworder="index">
   <object id="4" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="307">
  <objectgroup draworder="index">
   <object id="3" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="342" terrain="8,8,8,"/>
 <tile id="343" terrain="8,8,,8"/>
 <tile id="344" terrain=",,,8"/>
 <tile id="345" terrain=",,8,8"/>
 <tile id="346" terrain=",,8,"/>
 <tile id="353">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="354">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="361">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="362">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="399" terrain="8,,8,8"/>
 <tile id="400" terrain=",8,8,8"/>
 <tile id="401" terrain=",8,,8"/>
 <tile id="402" terrain="8,8,8,8"/>
 <tile id="403" terrain="8,,8,"/>
 <tile id="458" terrain=",8,,"/>
 <tile id="459" terrain="8,8,,"/>
 <tile id="460" terrain="8,,,"/>
 <tile id="470">
  <objectgroup draworder="index">
   <object id="1" x="4" y="16">
    <polygon points="0,0 8,0 8,-8 0,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="471">
  <objectgroup draworder="index">
   <object id="1" x="4" y="16">
    <polygon points="0,0 8,0 8,-8 0,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="513" terrain="9,9,9,"/>
 <tile id="514" terrain="9,9,,9"/>
 <tile id="515" terrain=",,,9"/>
 <tile id="516" terrain=",,9,9"/>
 <tile id="517" terrain=",,9,"/>
 <tile id="518" terrain="0,0,0,"/>
 <tile id="519" terrain="0,0,,0"/>
 <tile id="520" terrain=",,,0"/>
 <tile id="521" terrain=",,0,0"/>
 <tile id="522" terrain=",,0,"/>
 <tile id="527">
  <objectgroup draworder="index">
   <object id="1" x="4" y="16">
    <polygon points="0,0 8,0 4,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="528">
  <objectgroup draworder="index">
   <object id="1" x="4" y="16">
    <polygon points="0,0 8,0 4,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="536">
  <objectgroup draworder="index">
   <object id="1" x="4" y="16">
    <polygon points="0,0 8,0 4,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="563">
  <objectgroup draworder="index">
   <object id="8" x="4" y="8" width="8" height="8"/>
  </objectgroup>
 </tile>
 <tile id="570" terrain="9,,9,9"/>
 <tile id="571" terrain=",9,9,9"/>
 <tile id="572" terrain=",9,,9"/>
 <tile id="573" terrain="9,9,9,9"/>
 <tile id="574" terrain="9,,9,"/>
 <tile id="575" terrain="0,,0,0"/>
 <tile id="576" terrain=",0,0,0"/>
 <tile id="577" terrain=",0,,0"/>
 <tile id="578" terrain="0,0,0,0"/>
 <tile id="579" terrain="0,,0,"/>
 <tile id="589">
  <objectgroup draworder="index">
   <object id="1" x="0" y="16">
    <polygon points="0,0 16,0 16,-4 0,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="590">
  <objectgroup draworder="index">
   <object id="2" x="0" y="16">
    <polygon points="0,0 16,0 16,-4 0,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="591">
  <objectgroup draworder="index">
   <object id="2" x="0" y="16">
    <polygon points="0,0 16,0 16,-4 0,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="597">
  <objectgroup draworder="index">
   <object id="1" x="4" y="16">
    <polygon points="0,0 8,0 4,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="616">
  <objectgroup draworder="index">
   <object id="2" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="617">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="618">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="619">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="629" terrain=",9,,"/>
 <tile id="630" terrain="9,9,,"/>
 <tile id="631" terrain="9,,,"/>
 <tile id="634" terrain=",0,,"/>
 <tile id="635" terrain="0,0,,"/>
 <tile id="636" terrain="0,,,"/>
 <tile id="640">
  <objectgroup draworder="index">
   <object id="1" x="4" y="16">
    <polygon points="0,0 8,0 4,-8"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="641">
  <objectgroup draworder="index">
   <object id="1" x="4" y="16">
    <polygon points="0,0 8,0 4,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="642">
  <objectgroup draworder="index">
   <object id="1" x="4" y="16">
    <polygon points="0,0 8,0 4,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="643">
  <properties>
   <property name="unity:sortingLayerName" value="WorldObjects"/>
  </properties>
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
   <object id="2" x="4" y="16">
    <polygon points="0,0 8,0 4,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="644">
  <objectgroup draworder="index">
   <object id="1" x="4" y="16">
    <polygon points="0,0 8,0 4,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="645">
  <properties>
   <property name="unity:sortingLayerName" value="WorldObjects"/>
  </properties>
  <objectgroup draworder="index">
   <object id="2" x="4" y="16">
    <polygon points="0,0 8,0 4,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="646">
  <objectgroup draworder="index">
   <object id="2" x="0" y="16">
    <polygon points="0,0 16,0 16,-4 0,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="647">
  <objectgroup draworder="index">
   <object id="2" x="0" y="16">
    <polygon points="0,0 16,0 16,-4 0,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="648">
  <objectgroup draworder="index">
   <object id="2" x="0" y="16">
    <polygon points="0,0 16,0 16,-4 0,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="650">
  <objectgroup draworder="index">
   <object id="1" x="4" y="16">
    <polygon points="0,0 8,0 4,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="654">
  <objectgroup draworder="index">
   <object id="1" x="4" y="16">
    <polygon points="0,0 8,0 4,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="673">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="674">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="675">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="676">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="677">
  <objectgroup draworder="index">
   <object id="1" x="0" y="4" width="16" height="12"/>
  </objectgroup>
 </tile>
 <tile id="684" terrain="10,10,10,"/>
 <tile id="685" terrain="10,10,,10"/>
 <tile id="686" terrain=",,,10"/>
 <tile id="687" terrain=",,10,10"/>
 <tile id="688" terrain=",,10,"/>
 <tile id="697">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="698">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="699">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="700">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="701">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="741" terrain="10,,10,10"/>
 <tile id="742" terrain=",10,10,10"/>
 <tile id="743" terrain=",10,,10"/>
 <tile id="744" terrain="10,10,10,10"/>
 <tile id="745" terrain="10,,10,"/>
 <tile id="756">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="757">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="758">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
   <object id="2" x="-8" y="0"/>
  </objectgroup>
 </tile>
 <tile id="800" terrain=",10,,"/>
 <tile id="801" terrain="10,10,,"/>
 <tile id="802" terrain="10,,,"/>
 <tile id="811">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="855" terrain="2,2,2,"/>
 <tile id="856" terrain="2,2,,2"/>
 <tile id="857" terrain=",,,2"/>
 <tile id="858" terrain=",,2,2"/>
 <tile id="859" terrain=",,2,"/>
 <tile id="860" terrain="6,6,6,"/>
 <tile id="861" terrain="6,6,,6"/>
 <tile id="862" terrain=",,,6"/>
 <tile id="863" terrain=",,6,6"/>
 <tile id="864" terrain=",,6,"/>
 <tile id="912" terrain="2,,2,2"/>
 <tile id="913" terrain=",2,2,2"/>
 <tile id="914" terrain=",2,,2"/>
 <tile id="915" terrain="2,2,2,2"/>
 <tile id="916" terrain="2,,2,"/>
 <tile id="917" terrain="6,,6,6"/>
 <tile id="918" terrain=",6,6,6"/>
 <tile id="919" terrain=",6,,6"/>
 <tile id="920" terrain="6,6,6,6"/>
 <tile id="921" terrain="6,,6,"/>
 <tile id="971" terrain=",2,,"/>
 <tile id="972" terrain="2,2,,"/>
 <tile id="973" terrain="2,,,"/>
 <tile id="976" terrain=",6,,"/>
 <tile id="977" terrain="6,6,,"/>
 <tile id="978" terrain="6,,,"/>
 <tile id="1026" terrain="3,3,3,"/>
 <tile id="1027" terrain="3,3,,3"/>
 <tile id="1028" terrain=",,,3"/>
 <tile id="1029" terrain=",,3,3"/>
 <tile id="1030" terrain=",,3,"/>
 <tile id="1083" terrain="3,,3,3"/>
 <tile id="1084" terrain=",3,3,3"/>
 <tile id="1085" terrain=",3,,3"/>
 <tile id="1086" terrain="3,3,3,3"/>
 <tile id="1087" terrain="3,,3,"/>
 <tile id="1136">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12">
    <polygon points="0,0 16,0 8,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1137">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="1138">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="1139">
  <objectgroup draworder="index">
   <object id="2" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="1142" terrain=",3,,"/>
 <tile id="1143" terrain="3,3,,"/>
 <tile id="1144" terrain="3,,,"/>
 <tile id="1193">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12">
    <polygon points="0,0 16,0 8,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1194">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="1195">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="1196">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="1197" terrain="4,4,4,"/>
 <tile id="1198" terrain="4,4,,4"/>
 <tile id="1199" terrain=",,,4"/>
 <tile id="1200" terrain=",,4,4"/>
 <tile id="1201" terrain=",,4,"/>
 <tile id="1202" terrain="5,5,5,"/>
 <tile id="1203" terrain="5,5,,5"/>
 <tile id="1204" terrain=",,,5"/>
 <tile id="1205" terrain=",,5,5"/>
 <tile id="1206" terrain=",,5,"/>
 <tile id="1210">
  <objectgroup draworder="index">
   <object id="1" x="16" y="0">
    <polygon points="0,0 -16,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1211">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1214">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1215">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1216">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1217">
  <objectgroup draworder="index">
   <object id="1" x="0" y="16">
    <polygon points="0,0 16,-16 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1218">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,16 16,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1221">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1222">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1223">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1224">
  <objectgroup draworder="index">
   <object id="1" x="0" y="16">
    <polygon points="0,0 16,-16 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1225">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1231">
  <objectgroup draworder="index">
   <object id="1" x="0" y="16">
    <polygon points="0,0 16,-16 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1232">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1250">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12">
    <polygon points="0,0 16,0 8,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1251">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="1252">
  <objectgroup draworder="index">
   <object id="2" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="1253">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="1254" terrain="4,,4,4"/>
 <tile id="1255" terrain=",4,4,4"/>
 <tile id="1256" terrain=",4,,4"/>
 <tile id="1257" terrain="4,4,4,4"/>
 <tile id="1258" terrain="4,,4,"/>
 <tile id="1259" terrain="5,,5,5"/>
 <tile id="1260" terrain=",5,5,5"/>
 <tile id="1261" terrain=",5,,5"/>
 <tile id="1262" terrain="5,5,5,5"/>
 <tile id="1263" terrain="5,,5,"/>
 <tile id="1267">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1268">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,0 16,16 0,16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1271">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
   <object id="2" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1272">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1273">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1274">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1275">
  <objectgroup draworder="index">
   <object id="4" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1278">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1279">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1280">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1281">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1282">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1285">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1286">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
   <object id="2" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1287">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1288">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1289">
  <objectgroup draworder="index">
   <object id="2" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1292">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1293">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1294">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1308">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="1309">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="1310">
  <objectgroup draworder="index">
   <object id="1" x="0" y="12" width="16" height="4"/>
  </objectgroup>
 </tile>
 <tile id="1313" terrain=",4,,"/>
 <tile id="1314" terrain="4,4,,"/>
 <tile id="1315" terrain="4,,,"/>
 <tile id="1318" terrain=",5,,"/>
 <tile id="1319" terrain="5,5,,"/>
 <tile id="1320" terrain="5,,,"/>
 <tile id="1324">
  <objectgroup draworder="index">
   <object id="1" x="0" y="16">
    <polygon points="0,0 16,-16 0,-16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1325">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,16 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1328">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1329">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1330">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1331">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 0,16 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1332">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,16 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1335">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1336">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1337">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1339">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0">
    <polygon points="0,0 16,16 16,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1342">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1343">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1344">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1345">
  <objectgroup draworder="index">
   <object id="1" x="0" y="16">
    <polygon points="0,0 16,-16 0,-16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1346">
  <objectgroup draworder="index">
   <object id="1" x="16" y="16">
    <polygon points="0,0 -16,-16 0,-16"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1349">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1350">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
 <tile id="1351">
  <objectgroup draworder="index">
   <object id="1" x="0" y="0" width="16" height="16"/>
  </objectgroup>
 </tile>
</tileset>
