﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TouchControls : MonoBehaviour {

	public Vector2 moveInput;
	public bool attackDown;
	public bool speakDown;
	public bool speakUp;
	private float timeToSpeakUp;
	private float counterToSpeakUp;
	
	void Start () {
		moveInput = Vector2.zero;
		attackDown = false;
		speakDown = false;
		timeToSpeakUp = 1;
		counterToSpeakUp = timeToSpeakUp;
	}
	
	// Update is called once per frame
	void Update () {
		if(speakUp){
			counterToSpeakUp -= Time.deltaTime;
		
			if(counterToSpeakUp <= 0){
				speakUp = false;
				counterToSpeakUp = timeToSpeakUp;
			}
		}
	}
	
	public void playerLeftUIPointerDown(){
		moveInput = new Vector2(-1f, moveInput.y);
	}
	
	public void playerLeftUIPointerUp(){
		
		moveInput = new Vector2(0f, moveInput.y);
	}
	
	public void playerRightUIPointerDown(){
		
		moveInput = new Vector2(1f, moveInput.y);
	}
	
	public void playerRightUIPointerUp(){
		
		moveInput = new Vector2(0f, moveInput.y);
	}
	
	public void playerUpUIPointerDown(){
		
		moveInput = new Vector2(moveInput.x, 1f);
	}
	
	public void playerUpUIPointerUp(){
		
		moveInput = new Vector2(moveInput.x, 0f);
	}
	
	public void playerDownUIPointerDown(){
		
		moveInput = new Vector2(moveInput.x, -1f);
	}
	
	public void playerDownUIPointerUp(){
		
		moveInput = new Vector2(moveInput.x, 0f);
	}
	
	public void playerAttackUIPointerDown(){
		attackDown = true;
	}
	
	public void playerSpeakUIPointerUp(){
		speakUp = true;
	}
}
