﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {
	public GameObject dBox;
	public Text dText;
	
	public bool dialogActive;
	
	public string[] dialogLines;
	public int currentLine;
	
	private PlayerController thePlayer;
	
	private TouchControls theTC;

	void Start () {
		thePlayer = FindObjectOfType<PlayerController>();
		
		theTC = FindObjectOfType<TouchControls>();
	}
	
	void Update () {
		if(dialogActive && (Input.GetKeyUp(KeyCode.Space) || theTC.speakUp)){
			theTC.speakUp = false;
			currentLine++;
		}
		if(currentLine >= dialogLines.Length){
			dBox.SetActive(false);
			dialogActive = false;
			currentLine = 0;
			thePlayer.canMove = true;
		}
		if(currentLine < 0){
			currentLine = 0;
		}
		// Debug.Log("Current line: " + currentLine);
		dText.text = dialogLines[currentLine];
	}
	
	public void ShowBox(string dialogue){
		dialogActive = true;
		dBox.SetActive(true);
		dText.text = dialogue;
		thePlayer.canMove = false;
	}
	
	public void ShowDialogue(){
		dialogActive = true;
		dBox.SetActive(true);
		thePlayer.canMove = false;
		
	}
}
