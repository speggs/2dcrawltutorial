﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float moveSpeed;
	private float currentMoveSpeed;

	private Animator anim;
	private Rigidbody2D myRigidbody;

	public bool playerMoving;
	public Vector2 lastMove;
	private Vector2 moveInput;

	private static bool playerExists;
	
	private bool attacking;
	public float attackTime;
	private float attackTimeCounter;
	
	public string startPoint;
	
	public bool canMove;
	
	private SFXManager sfxMan;
	
	private TouchControls theTC;
	
	void Start () {
		anim = GetComponent<Animator> ();
		myRigidbody = GetComponent<Rigidbody2D> ();
		sfxMan = FindObjectOfType<SFXManager>();
		theTC = GetComponent<TouchControls>();
		
		if (!playerExists) {
			playerExists = true;
			DontDestroyOnLoad(transform.gameObject);
		} else {
			Destroy (gameObject);
		}
		canMove = true;
	}

	void Update () {
		playerMoving = false;
		
		//Set in Dialog manager
		if(!canMove){
			myRigidbody.velocity = Vector2.zero;
			return;
		}
		if(!attacking){
			// if(Input.GetAxisRaw("Horizontal")> 0.5f || Input.GetAxisRaw("Horizontal") < -0.5f){
				// //transform.Translate(new Vector3(Input.GetAxisRaw("Horizontal") * moveSpeed * Time.deltaTime,0f,0f));
				// myRigidbody.velocity = new Vector2(Input.GetAxisRaw("Horizontal")*currentMoveSpeed, myRigidbody.velocity.y);
				// playerMoving = true;
				// lastMove = new Vector2(Input.GetAxisRaw ("Horizontal"), 0f);
			// }else{
				 // myRigidbody.velocity = new Vector2(0f, myRigidbody.velocity.y);
			// }

			// if(Input.GetAxisRaw("Vertical")> 0.5f || Input.GetAxisRaw("Vertical") < -0.5f){
				// //transform.Translate(new Vector3(0f, Input.GetAxisRaw("Vertical") * moveSpeed * Time.deltaTime,0f));
				// myRigidbody.velocity = new Vector2( myRigidbody.velocity.x, Input.GetAxisRaw("Vertical")*currentMoveSpeed);
				// playerMoving = true;
				// lastMove = new Vector2(0f, Input.GetAxisRaw ("Vertical"));

			// }else{
				// myRigidbody.velocity = new Vector2(myRigidbody.velocity.x, 0f);
				
			// }
			//#if UNITY_ANDROID
			//moveInput = new Vector2(theTC.moveInput.x, theTC.moveInput.y).normalized;
			//#else
			moveInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical")).normalized;
			if(moveInput == Vector2.zero){
				moveInput = new Vector2(theTC.moveInput.x, theTC.moveInput.y).normalized;
			}
			//#endif
			if(moveInput != Vector2.zero){
				myRigidbody.velocity = new Vector2(moveInput.x*moveSpeed, moveInput.y*moveSpeed);
				playerMoving = true;
				lastMove = moveInput;
			}else{
				myRigidbody.velocity = Vector2.zero;
			}
			
			if(Input.GetKeyDown(KeyCode.J) || theTC.attackDown){
				attackTimeCounter = attackTime;
				attacking = true;
				anim.SetBool("Attack",true);
				myRigidbody.velocity = Vector2.zero;
				theTC.attackDown = false;
				
				sfxMan.playerAttack.Play();
			}
			
			// if(Mathf.Abs(Input.GetAxisRaw("Horizontal")) > 0.5f && Mathf.Abs(Input.GetAxisRaw("Vertical")) > 0.5f){
				// currentMoveSpeed = moveSpeed*0.75f;
			// }else{
				// currentMoveSpeed = moveSpeed;
			// }
		}
		if(attackTimeCounter > 0){
			attackTimeCounter -= Time.deltaTime;
		}else{
			attacking = false;
			anim.SetBool("Attack", false);
		}
		
		if (!playerMoving) {
			myRigidbody.velocity = new Vector2 (0f, 0f);
		}
		//anim.SetFloat ("MoveX", Input.GetAxisRaw ("Horizontal"));
		//anim.SetFloat ("MoveY", Input.GetAxisRaw ("Vertical"));
		anim.SetFloat ("MoveX", moveInput.x);
		anim.SetFloat ("MoveY", moveInput.y);
		anim.SetBool("PlayerMoving", playerMoving);
		anim.SetFloat("LastMoveX", lastMove.x);
		anim.SetFloat("LastMoveY", lastMove.y);
	}
		
	public void Grow(){
		transform.localScale += new Vector3(1f, 1f, 0f);
		FindObjectOfType<CameraController>().UpdateOrthSize(7);
		moveSpeed *= 1.5f;
	}
}
