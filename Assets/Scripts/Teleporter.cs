﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour {

	private PlayerController thePlayer;
	private CameraController theCamera;
	
	public Teleporter destination;
	
	void Start () {
		thePlayer = FindObjectOfType<PlayerController>();
		theCamera = FindObjectOfType<CameraController>();
	}
	
	void Update () {
		
	}
	
	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.name == "Player") {
			if(thePlayer.lastMove.x > 0){
				thePlayer.transform.position = new Vector3 (destination.transform.position.x + 3, destination.transform.position.y, 0);
				theCamera.transform.position = new Vector3 (thePlayer.transform.position.x, thePlayer.transform.position.y, theCamera.transform.position.z);
			}else if(thePlayer.lastMove.x < 0){
				
				thePlayer.transform.position = new Vector3 (destination.transform.position.x - 3, destination.transform.position.y, 0);
				theCamera.transform.position = new Vector3 (thePlayer.transform.position.x, thePlayer.transform.position.y, theCamera.transform.position.z);
			}else if(thePlayer.lastMove.y > 0){
				
				thePlayer.transform.position = new Vector3 (destination.transform.position.x, destination.transform.position.y + 3, 0);
				theCamera.transform.position = new Vector3 (thePlayer.transform.position.x, thePlayer.transform.position.y, theCamera.transform.position.z);
			}else{
				
				thePlayer.transform.position = new Vector3 (destination.transform.position.x, destination.transform.position.y - 3, 0);
				theCamera.transform.position = new Vector3 (thePlayer.transform.position.x, thePlayer.transform.position.y, theCamera.transform.position.z);
			}
		}
	}
}
