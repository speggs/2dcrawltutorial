﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LoadNewArea : MonoBehaviour {

	public string levelToLoad;
	
	public string exitPoint;
	
	private PlayerController thePlayer;
	
	void Start () {
		thePlayer = FindObjectOfType<PlayerController>();
	}
	
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.name == "Player") {
			//Application.LoadLevel (levelToLoad);
			SceneManager.LoadScene(levelToLoad,LoadSceneMode.Single);
						
			thePlayer.startPoint = exitPoint;
		}
	}
}
