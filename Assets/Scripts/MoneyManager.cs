﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MoneyManager : MonoBehaviour {

	public Text moneyText;
	public int currentGold;
	
	void Start () {
		
		if(PlayerPrefs.HasKey("CurrentMoney")){
			currentGold = PlayerPrefs.GetInt("CurrentMoney");
		}else{
			currentGold = 0;
			PlayerPrefs.SetInt("CurrentMoney", currentGold);
		}
		moneyText.text = "Gold: " + currentGold;
	}
	
	void Update () {
		
	}
	
	public void addMoney(int goldToAdd){
		currentGold += goldToAdd;
		moneyText.text = "Gold: " + currentGold;
		PlayerPrefs.SetInt("CurrentMoney", currentGold);
	}
}
