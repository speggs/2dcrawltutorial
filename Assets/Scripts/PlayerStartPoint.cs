﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStartPoint : MonoBehaviour {

	private PlayerController thePlayer;
	private CameraController theCamera;

	public Vector2 startDirection;
	
	public string pointName;

	void Start () {
		thePlayer = FindObjectOfType<PlayerController>();
				
		if(thePlayer.startPoint == pointName){
			thePlayer.transform.position = transform.position;

			//Debug.Log("PT " + pointName + ": position set to " + transform.position.x+ ", " + transform.position.y);
			
			thePlayer.lastMove = startDirection;
			thePlayer.playerMoving = false;


			theCamera = FindObjectOfType<CameraController>();
			theCamera.transform.position = new Vector3 (transform.position.x, transform.position.y, theCamera.transform.position.z);
		}
	}

	void Update () {
		
	}
}
