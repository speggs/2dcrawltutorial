﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthManager : MonoBehaviour {
	public int MaxHealth;
	public int CurrentHealth;
	
	private PlayerStats thePlayerStats;
	
	public int expToGive;
	
	public string enemyQuestName;
	private QuestManager theQM;

	//
	void Start () {
		CurrentHealth = MaxHealth;
		thePlayerStats = FindObjectOfType<PlayerStats>();
		
		theQM = FindObjectOfType<QuestManager>();
	}
	
	void Update () {
		if(CurrentHealth <= 0){
			theQM.enemyKilled = enemyQuestName;
			
			Destroy(gameObject);
			
			thePlayerStats.addExperience(expToGive);
		}
	}
	
	public void HurtEnemy(int damageToGive){
		CurrentHealth -= damageToGive;
		
	}
	
	public void SetMaxHealth(){
		CurrentHealth = MaxHealth;
	}
}